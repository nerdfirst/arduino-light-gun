# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Building an Arduino Light Gun**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/drh4p4UeykI/0.jpg)](https://www.youtube.com/watch?v=drh4p4UeykI "Click to Watch")


# Errata In Video

![Errata Image](CorrectedSchematic.jpg)


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.