var WebSocketServer = require("websocket").server;
var http = require("http");
var SerialPort = require("serialport").SerialPort;

var clients = [];

var httpServer = http.createServer(function(request, response) {
	
});
httpServer.listen(8612, function() {});


wsServer = new WebSocketServer({
	"httpServer": httpServer
});
wsServer.on("request", function(request) {
	var conn = request.accept();
	clients.push(conn);
	
	conn.on("message", function(msg) {
		serial.write("PING");
	});
});


var serial = new SerialPort("COM18", {
	baudRate: 115200,
	parser: SerialPort.parsers.readline("\n")
});
serial.on("data", function(data) {
	for (client of clients) {
		client.sendUTF(data);
	}
});