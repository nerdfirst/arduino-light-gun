int last;

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(10);
  last = HIGH;
}

void loop() {
  int button = digitalRead(A0);
  if (button == LOW && last == HIGH) {
     Serial.println("TRIGGER");
  }  

  if (Serial.available() > 0) {
    Serial.readString();
    Serial.println(analogRead(A1));
  }

  last = button;
}
